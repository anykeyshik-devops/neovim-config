-- tab movement
local opts = {noremap = true, silent = true}
vim.api.nvim_set_keymap('', 'H', '<Cmd>BufferPrevious<CR>', opts)
vim.api.nvim_set_keymap('', 'L', '<Cmd>BufferNext<CR>', opts)
vim.api.nvim_set_keymap('', 'X', '<Cmd>BufferClose<CR>', opts)
